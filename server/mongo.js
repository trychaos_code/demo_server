const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'tannu_abhi';

let dbObj;
MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");
    dbObj = client.db(dbName);
});

module.exports = {
    getData: function (query, collection, cb) {
        dbObj.collection(collection).find(query).toArray(function(err, result) {
            cb(result);
        });
    },
    insertData: function (data, collection, cb) {
        dbObj.collection(collection).insertMany(data, cb);
    },
    updateData: function (query, newVal, collection, cb) {
        dbObj.collection(collection).updateOne(query, {$set: newVal}, { upsert: true }, function(err, result) {
            cb(err, result);
            console.log('newVal: ', query, newVal)
        });
    },
    deleteData: function (query, collection, cb) {
        dbObj.collection(collection).deleteOne(query, function(err, result) {
            cb(result);
        });
    }
};
