import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// constants data
import { END_POINTS } from './endpoints';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class DataService {
  constructor(private http: HttpClient){

  }
  getEmployee (cb) {
    this.http.get(END_POINTS.BASE+END_POINTS.GET_EMPLOYEE).subscribe(data=>{
      console.log('jhyagkuewgfui: ', data);
      cb(data);
    })
  }
  insertEmployee (data, cb) {
    this.http.put(END_POINTS.BASE+END_POINTS.INSERT_EMPLOYEE,
      data, httpOptions).subscribe(data=>{
      console.log('jhyagkuewgfui: ', data);
      cb(data);
    })
  }
  deleteEmployee (query, cb) {
    this.http.post( END_POINTS.BASE+END_POINTS.DELETE_EMPLOYEE,
      query, httpOptions).subscribe(res=>{
        cb(res)
    })
  }


  updateEmployee (query, cb) {
    this.http.post(END_POINTS.BASE+END_POINTS.UPDATE_EMPLOYEE,
      query, httpOptions).subscribe(data=>{
      console.log('jhyagkuewgfui: ', data);
      cb(data);
    })
  }
}

