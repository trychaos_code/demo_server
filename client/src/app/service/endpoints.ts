export const END_POINTS = {
  BASE: 'http://localhost:5001/',
  GET_EMPLOYEE: 'get_employee',
  INSERT_EMPLOYEE: 'insert_employee',
  DELETE_EMPLOYEE: 'delete_employee',
  UPDATE_EMPLOYEE: 'update_employee'
};
